# Dockerschulung

## Video-Link
Hier siehst Du einen Probedurchlauf durch unsere Dockerschulung. Er diente in erster Linie für mich dazu, die Zeit abzuschätzen, kann aber auch als "Nachschlagemöglichkeit" verwendet werden, um eventuelle Unklarheiten im Nachhinein zu beseitigen - so hoffe ich :)

Youtube: https://www.youtube.com/watch?v=zCe6PMq5hGc

Auch super: https://www.youtube.com/DESdVoKhIxY (Golo Roden)

## Basics

### Alles aufräumen
* `docker system prune --all --volumes`

### Container starten
* `docker run hello-world`
* `docker run docker/whalesay cowsay Hello IKS!`
* `docker run -it dyego/snake-game`
* `docker run --rm pythonistda` ohne Caching
* `docker run -p 8080:8080 kodekloud/simple-webapp`
* `docker run -p 8080:8080 kodekloud/simple-webapp --red`
* `docker run -p 8080:8080 -d --name halloservername halloserver`

### Images pullen
* `docker pull quay.io/bitnami/mongodb`

### Images anzeigen
* `docker images`

### Images entfernen
* `docker rmi <imageId>`
* `docker rmi -f $(docker images -q)` alle Images löschen

### Container anzeigen
* `docker ps` nur laufende
* `docker ps -a` auch gestoppte
* `docker ps -a -f ancestor=pythonistda` alle, die auf pythonistda basieren.

### Container stoppen und starten
* `docker stop <Container-ID/-Name>` (graceful: SIGTERM)
* `docker kill <Container-ID/-Name>` (hard: SIGKILL)
* `docker start <Container-ID/-Name>`

Beispiele:
* `docker stop 28b4b25bd513`
* `docker start -i flamboyant_hawking`

### Container entfernen
* `docker rm <Container-ID/-Name>`
* `docker rm -f <Container-ID/-Name>` (stoppt mit SIGKILL)
* `docker rm $(docker ps -aq)` alle gestoppten Container entfernen
* `docker rm -f $(docker ps -aq)` alle Container hart entfernen
* `docker rm $(docker ps -aq -f ancestor=pythonistda)`

### Genau betrachten
* `docker inspect pythonistda`
 
### Logging
* `docker logs <Container-ID/-Name>`

Beispiele:
* `docker logs 32c63e76c49a`
* `docker logs gifted_williams`

### In einen Container hineinschauen
* `docker run -it python /bin/bash`
* `docker exec -it webserver /bin/bash` in einen laufenden
    
## Dockerfiles
```
FROM ubuntu
CMD echo "Ich will hieraus"
```
```
FROM ubuntu
ENTRYPOINT echo "Ich will hieraus"
```
```
FROM alpine
CMD echo "Ich will hieraus"; sleep 10
```
```
FROM alpine
CMD ["echo", "Ich will hieraus"]
```
```
FROM python
ENTRYPOINT echo "print('Python ist da')" | python3
```
```
FROM python
CMD ["/bin/sh", "-c", "echo \"print('Python ist da')\" | python3"]
```
```
FROM python
COPY index.html .
ENTRYPOINT ["python3", "-m", "http.server", "8080"]
```
```
FROM python
COPY index.html /var/www/
WORKDIR /var/www/
ENTRYPOINT ["python3", "-m", "http.server", "8080"]
```
```
FROM python
RUN mkdir /var/www/
WORKDIR /var/www/
ENTRYPOINT ["python3", "-m", "http.server"]
CMD ["8080"]
```
```
FROM tomcat 
COPY helloworld.war /usr/local/tomcat/webapps
ENV JAVA_OPTS="-Xms512M -Xmx1024M"
```
```
FROM alpine
RUN apk add zip
```

### Image bauen
* `docker build -t webserver .`

### Build-Argumente und Labels
```
FROM python
LABEL maintainer="dev@someproject.org"
ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS
ENV BUILD_VERSION=$BUILD_VERSION
LABEL build_date=$BUILD_DATE
LABEL build_version=$BUILD_VERSION 
LABEL vcs=$VCS
ENTRYPOINT ["python3", "-m", "http.server", "8080"]
```

```
docker build \
 --build-arg "BUILD_DATE=$(date)" \
 --build-arg "BUILD_VERSION=1.0.2"  \
 --build-arg "VCS=https://github.com/ballerinalang/container-support"  \
 -t webserver .
```

### Volumes
* `docker run -v $(pwd):/var/www -p 9090:8080 webserver
`
## Registries
* `docker login` DockerHub
* `docker login docker.io` DockerHub 
* `docker login registry.gitlab.com` GitLab

### Pushen
* `docker push javacook/poc-aboverwaltung:1.0` DockerHub
* `docker push docker.io/javacook/poc-aboverwaltung:1.0` DockerHub
* `docker push registry.gitlab.com/dockerschulung/joerg/myimage` GitLab

### Taggen
* `docker tag mytomcat javacook/mytomcat`
* `docker tag mytomcat javacook/mytomcat:1.0`

## Zwei Container im Netzwerk
* `docker network create hallonet`
* `docker network connect hallonet halloservername`
* `docker run -it --network hallonet halloclient`
* `docker network create mongo-network`
* ```
  docker run -d -p 27017:27017 \
  -e MONGO_INITDB_ROOT_USERNAME=admin \
  -e MONGO_INITDB_ROOT_PASSWORD=password \
  --name mongodb \ 
  --net mongo-network \
  mongo
  ```
* ```
  docker run -d \ 
  --network mongo-network \ 
  --name mongo-express \ 
  -p 8081:8081 \ 
  -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \ 
  -e ME_CONFIG_MONGODB_ADMINPASSWORD=password \ 
  -e ME_CONFIG_MONGODB_SERVER=mongodb \ 
  mongo-express
  ```
  
### JIB
`git clone https://gitlab.com/dockerschulung/spring-reactive-mongo-crud.git`
```
...
<plugin>  
    <groupId>com.google.cloud.tools</groupId>      
    <artifactId>jib-maven-plugin</artifactId>   
    <version>3.1.4</version>   
    <configuration>     
        <to>        
            <image>registry.gitlab.com/dockerschulung/joerg/reactivemongo:1.0</image>     
        </to>  
    </configuration>
</plugin>
...
```
settings.xml:
```
<server>    
    <id>registry.gitlab.com</id>    
    <username>dockerschulung</username>    
    <password>docker20211122</password>    
    <configuration>        
        <email>docker-schulung@gmx.de</email>    
    </configuration>
</server>
```
